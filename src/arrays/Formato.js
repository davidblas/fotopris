const formatos = [
       { id: 1,formato: "20x30", tipo: "Rectangular" , precio:24.99},
       { id: 2,formato: "30x40", tipo: "Rectangular" , precio:34.99},
       { id: 3,formato: "40x50", tipo: "Rectangular" , precio:49.99},
       { id: 4,formato: "40x60", tipo: "Rectangular" , precio:54.99},
       { id: 5,formato: "50x75", tipo: "Rectangular" , precio:69.99},
       { id: 6,formato: "60x80", tipo: "Rectangular" , precio:79.99},
       { id: 7,formato: "60x90", tipo: "Rectangular" , precio:84.99},
       { id: 8,formato: "75x100", tipo: "Rectangular" , precio:99.99},

       { id: 9,formato: "30x60", tipo: "Panoramico" , precio:44.99},
       { id: 10,formato: "30x90", tipo: "Panoramico" , precio:59.99},
       
       { id: 11,formato: "20x20", tipo: "Cuadrado" , precio:7.00},
       { id: 12,formato: "30x30", tipo: "Cuadrado" , precio:29.99},
       { id: 13,formato: "40x40", tipo: "Cuadrado" , precio:39.99},
       { id: 14,formato: "50x50", tipo: "Cuadrado" , precio:56.99},
       { id: 15,formato: "60x60", tipo: "Cuadrado" , precio:64.99},
       { id: 16,formato: "80x80", tipo: "Cuadrado" , precio:89.99},
      
    
   ]    


   export default formatos;