import { useState } from 'react';
import { Button, Card, Container, Row, Col, Modal, Form } from 'react-bootstrap'
import formatos from '../arrays/Formato'




function MuestraCarta({ carta }) {

    const [vistasformato, setVistasformato] = useState([]);


    const [formatoEscogido, setFormatoEscogido] = useState(formatos[0]); //estado de precio seleccionado

    //config modal.
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const tamanyo = ["Cuadrado", "Panoramico", "Rectangular"]

    let medidas = tamanyo.map((e) => {

        return (

            <Button onClick={() => filtraformato(e)} className="btn-formatos" variant="outline-dark">{e}</Button>

        )
    })

    // Filtra los tipos del array : clasico ,panoramico,rectangular
    function filtraformato(medidas) {
        let f = formatos.filter((elemento) => {
            return elemento.tipo === medidas;


        })

        let vistas = f.map((f, i) => {
            return (<Button key={i} onClick={() => setFormatoEscogido(f)} className={f.id === formatoEscogido.id ? "button-size activo" : "button-size"} variant="outline-dark">
                {f.formato}</Button>)
        })
        setVistasformato(vistas);


    }
    return (
        
           

        <div className="container lg-4 ">

                <Card className="estilocard" style={{ width: '14rem' }}  >
                    <Card.Img className="image-card" variant="top" src={carta.foto} />
                    <Card.Body>
                        <Card.Title className=" titulocarta"><h3>Poster simple</h3></Card.Title>
                        <p>Lienzo tipo: {carta.categoria} </p>
                        <hr />
                        <Card.Text>
                            <p>Imprime tu foto favorita de un borde a otro de tu lienzo</p>

                            <div className='tamanyo'>

                                <Modal show={show} onHide={handleClose} animation={false} >
                                    <Modal.Header closeButton>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <div className="card-container" style={{ justifyContent: 'center' }}>
                                            <img className="img-card" src={carta.foto}></img>
                                        </div>
                                        <br />

                                        <div className="dimensiones-formatos">
                                            <h4>Escoge el formato adecuado:</h4>

                                            <div className="structure-format">
                                                {medidas}

                                            </div>

                                            <hr />
                                            <h4 className="titulo tamanyo">Escoge el tamaño del lienzo:</h4>
                                            <div className="btn-formato">
                                                {vistasformato}
                                                <hr />
                                            </div>
                                            <p>Lienzo tipo : {carta.categoria}</p>
                                        </div>
                                        <h3>{formatoEscogido.precio}€</h3>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Cerrar
                                        </Button>
                                        <Button variant="dark" onClick={handleClose}>
                                            Añadir al carrito
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            </div>
                        </Card.Text>
                        <Button className="vertamanyo" variant="dark" onClick={handleShow} >Ver tamaños</Button>
                    </Card.Body>
                </Card>
                </div>
          
       


    );
}


export default MuestraCarta;