import{Navbar, Nav, Container, NavDropdown} from 'react-bootstrap'
import { FaCartPlus } from 'react-icons/fa';
import Cestacompra from './Cestacompra';

function Barranav(){

    return(
      <Navbar fixed  collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Container>
  <Navbar.Brand href="#home">FOTOPRISE</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">
      <Nav.Link href="foto/lienzo"> carrito {Cestacompra}</Nav.Link>
      <Nav.Link href="#pricing">Albumes</Nav.Link>
      <NavDropdown  title="Ideas para regalar" id="collasible-nav-dropdown">
        <NavDropdown.Item variant ="dark" href="#action/3.1">Regalos</NavDropdown.Item>
        <NavDropdown.Item variant ="dark" href="#action/3.2">Tazas</NavDropdown.Item>
        <NavDropdown.Item variant ="dark" href="#action/3.3">Targeta regalo</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Ideas de regalo</NavDropdown.Item>
      </NavDropdown>
    </Nav>
    <Nav>
      <Nav.Link href="#deets">More deets</Nav.Link>
      <Nav.Link eventKey={2} href="#memes">
        <div className="Iconcompra"> 
      <FaCartPlus style={{fontSize:'25px'}}/>
        </div>

      </Nav.Link>
    </Nav>
  </Navbar.Collapse>
  </Container>
</Navbar>

    )
}

export default Barranav;
