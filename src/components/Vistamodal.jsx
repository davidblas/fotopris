import MuestraCarta from './MuestraCarta'

import products from '../arrays/Products'




function Vistamodal({ }) {

    return (
        <div>
            <div className="row">

                {products.map((carta, i) => <MuestraCarta key={i} carta={carta} />)}

            </div>
        </div>

    )
}

export default Vistamodal;